(function($){
    
    "use strict";
    
    $(window).on('scroll', function(event) {
        if($(this).scrollTop() > 20){
            $('.back-interstitial').fadeOut(200)
        } else{
            $('.back-interstitial').fadeIn(200)
        }
    });
    
}(jQuery));
